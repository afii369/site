DB = 'sqlite:///../server/escargot.sqlite'
STATS_DB = 'sqlite:///../server/stats.sqlite'
DEBUG = False
PORT = 8080

# Set this in settings_local to use reCAPTCHA
RECAPTCHA = {
	'api_key': None,
	'secret_key': None,
}

# Set this in `settings_local` to use Mailjet
MAILJET = {
	'api_key': None,
	'secret_key': None,
}

try:
	from settings_local import *
except ImportError:
	raise Exception("Please create settings_local.py")
