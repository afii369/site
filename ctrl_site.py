import re
import jinja2
from aiohttp import web
from functools import lru_cache

from db import Session, User
from util import hash
from util.auth import AuthService
from util.misc import gen_uuid
import settings

def create_app(*, serve_static = False):
	app = App()
	
	app.router.add_get('/', page_index)
	app.router.add_get('/stats', page_stats)
	app.router.add_get('/forgot', page_forgot)
	app.router.add_post('/forgot', page_forgot)
	app.router.add_get('/reset/{token}', page_reset)
	app.router.add_post('/reset/{token}', page_reset)
	app.router.add_get('/etc/MsgrConfig', page_msgr_config)
	app.router.add_post('/etc/MsgrConfig', page_msgr_config)
	app.router.add_get('/news', page_news)
	app.router.add_get('/etc/escargot-today', page_news)
	app.router.add_get('/faq', page_faq)
	app.router.add_get('/downloads', page_downloads)
	app.router.add_get('/register', page_register)
	app.router.add_post('/register', page_register)
	app.router.add_get('/patching', page_patching)
	if serve_static:
		app.router.add_static('/static', 'static')
	app.router.add_route('*', '/{path:.*}', handle_404)
	app.jinja_env = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	return app

class App(web.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.stats = None
		self.auth_service = AuthService()
	
	async def startup(self):
		self.loop.create_task(self.sync_stats())
	
	async def sync_stats(self):
		import asyncio
		from datetime import datetime, timedelta
		from itertools import groupby
		import stats
		
		while True:
			now = datetime.utcnow()
			five_minutes_ago = now - timedelta(minutes = 5)
			hour_cutoff = now.timestamp() // 3600 - 24
			with stats.Session() as sess:
				stat = sess.query(stats.CurrentStats).filter(
					stats.CurrentStats.key == 'logged_in',
					stats.CurrentStats.date_updated > five_minutes_ago
				).one_or_none()
				logged_in = getattr(stat, 'value', 0)
				clients_by_id = {
					row.id: row.data
					for row in sess.query(stats.DBClient).all()
				}
				by_hour = sorted([
					{
						'hour': hcs.hour,
						'hour_formatted': _format_hour(hcs.hour),
						'client_formatted': _format_client(clients_by_id.get(hcs.client_id)),
						'users_active': int(hcs.users_active),
						'messages_sent': hcs.messages_sent,
						'messages_received': hcs.messages_received,
					}
					for hcs in sess.query(stats.HourlyClientStats).filter(stats.HourlyClientStats.hour >= hour_cutoff).all()
				], key = lambda x: (-x['hour'], -x['users_active']))
				by_hour = [
					(hour_formatted, list(l))
					for hour_formatted, l in groupby(by_hour, lambda x: x['hour_formatted'])
				]
			self.stats = {
				'logged_in': logged_in,
				'by_hour': by_hour,
			}
			await asyncio.sleep(300)

def _format_hour(hour):
	from datetime import datetime
	dt = datetime.fromtimestamp(hour * 3600)
	return dt.strftime("%Y-%m-%d, %H:00 - %H:59 UTC")

def _format_client(client):
	if client is None:
		return "(Unknown)"
	p = client['program']
	v = client['version']
	if p == 'msn' and v.startswith('MSNP'):
		v = _guess_msn_version(int(v[4:]))
	s = "{} {}".format(p.upper(), v)
	if client['via'] != 'direct':
		s += ", {}".format(client['via'].upper())
	return s

def _guess_msn_version(dialect):
	if dialect <= 2:
		return "1.?"
	if dialect <= 4:
		return "2.?"
	if dialect <= 5:
		return "3.?"
	if dialect <= 7:
		return "4.?"
	return "?/MSNP" + str(dialect)

async def page_stats(req):
	return render(req, 'stats.html', { 'stats': req.app.stats })

async def page_index(req):
	return render(req, 'index.html')

async def page_register(req):
	errors = None
	email = None
	created_email = None
	support_old = None
	if req.method == 'POST':
		data = await req.post()
		
		valid_recaptcha = await check_recaptcha(req, data.get('g-recaptcha-response') or '')
		if not valid_recaptcha:
			errors = { 'recaptcha': "Invisible reCAPTCHA failed." }
		
		if not errors:
			email = data.get('email') or ''
			pw1 = data.get('password1') or ''
			pw2 = data.get('password2') or ''
			support_old = (data.get('support_old') == 'true')
			errors = create_user(email, pw1, pw2, support_old)
		
		if not errors:
			return web.HTTPFound('/register?created_email={}'.format(email))
	else:
		created_email = req.query.get('created_email')
	
	return render(req, 'register.html', {
		'errors': errors,
		'email': email,
		'support_old': support_old,
		'created_email': created_email,
		'recaptcha_api_key': settings.RECAPTCHA['api_key'],
	})


async def handle_404(req):
	return render(req, '404.html', status = 404)

async def check_recaptcha(req, recaptcha_response):
	if not settings.RECAPTCHA['secret_key']:
		return True
	
	import aiohttp
	async with aiohttp.ClientSession() as session:
		req = session.post('https://www.google.com/recaptcha/api/siteverify', json = {
			'secret': settings.RECAPTCHA['secret_key'],
			'response': recaptcha_response,
			'remoteip': req.remote,
		})
		async with req as resp:
			resp = await resp.json()
	return resp['success']

async def page_msgr_config(req):
	if req.method == 'POST':
		body = await req.read()
	else:
		body = None
	msgr_config = _get_msgr_config(req, body)
	if msgr_config == 'INVALID_VER':
		return web.Response(status = 500)
	return web.HTTPOk(content_type = 'text/xml', text = msgr_config)

async def page_news(req):
	return render(req, 'news.html')

def _get_msgr_config(req, body):
	query = req.query
	result = None
	
	if query.get('ver') is not None:
		if re.match(r'[^\d\.]', query.get('ver')):
			return 'INVALID_VER'
		
		config_ver = query.get('ver').split('.', 4)
		if 5 <= int(config_ver[0]) <= 7:
			with open('MsgrConfig.msn.envelope.xml') as fh:
				envelope = fh.read()
			with open('MsgrConfig.msn.xml') as fh:
				config = fh.read()
			with open('MsgrConfig.tabs.xml') as fh:
				config_tabs = fh.read()
			result = envelope.format(MsgrConfig = config.format(tabs = config_tabs))
		elif int(config_ver[0]) == 8:
			with open('MsgrConfig.wlm.8.xml') as fh:
				config = fh.read()
			with open('MsgrConfig.tabs.xml') as fh:
				config_tabs = fh.read()
			result = config.format(tabs = config_tabs)
	elif body is not None:
		with open('MsgrConfig.msn.envelope.xml') as fh:
			envelope = fh.read()
		with open('MsgrConfig.msn.xml') as fh:
			config = fh.read()
		with open('MsgrConfig.tabs.xml') as fh:
			config_tabs = fh.read()
		result = envelope.format(MsgrConfig = config.format(tabs = config_tabs))
	
	return result or ''

async def page_forgot(req):
	errors = None
	email = None
	sent_to = None
	if req.method == 'POST':
		data = await req.post()
		email = data.get('email') or ''
		errors = send_password_reset(email, req.app.auth_service)
		if not errors:
			return web.HTTPFound('/forgot?sent_to={}'.format(email))
	else:
		sent_to = req.query.get('sent_to')
	
	return render(req, 'forgot.html', {
		'errors': errors,
		'email': email,
		'sent_to': sent_to,
	})

async def page_reset(req):
	auth_service = req.app.auth_service
	
	token = req.match_info.get('token', '')
	email = auth_service.get_token(PURPOSE_PWRESET, token)
	valid_token = (email is not None)
	errors = None
	reset_success = False
	support_old = None
	
	if valid_token:
		if req.method == 'POST':
			data = await req.post()
			pw1 = data.get('password1') or ''
			pw2 = data.get('password2') or ''
			support_old = (data.get('support_old') == 'true')
			errors = change_password(email, pw1, pw2, support_old)
			if not errors:
				auth_service.pop_token(PURPOSE_PWRESET, token)
				reset_success = True
		else:
			with Session() as sess:
				user = _get_user(email)
				if user:
					support_old = user.get_front_data('msn', 'pw_md5')
	
	return render(req, 'reset.html', {
		'valid_token': valid_token,
		'errors': errors,
		'reset_success': reset_success,
		'support_old': support_old,
	})

async def page_faq(req):
	return render(req, 'faq.html')

async def page_downloads(req):
	return render(req, 'downloads.html')

async def page_patching(req):
	return render(req, 'patching.html')

def create_user(email, pw1, pw2, support_old):
	errors = {}
	_check_email(errors, email)
	_check_passwords(errors, pw1, pw2)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is not None:
			# "Email already in use."
			errors['email'] = 'used'
			return errors
		user = User(
			uuid = gen_uuid(), email = email, verified = False,
			name = email, message = '',
			settings = {}, groups = {}, contacts = {},
		)
		_set_passwords(user, pw1, support_old)
		sess.add(user)
	
	return errors

def send_password_reset(email, auth_service):
	errors = {}
	_check_email(errors, email)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is None:
			# "Email not registered with Escargot. Did you sign up?"
			errors['email'] = 'not-registered'
			return errors
		token = auth_service.create_token(PURPOSE_PWRESET, email, lifetime = 3600)
		sent = _send_password_reset_email(email, token)
		if not sent:
			auth_service.pop_token(PURPOSE_PWRESET, token)
			# "Email could not be sent."
			errors['email'] = 'not-sent'
	
	return errors

def change_password(email, pw1, pw2, support_old):
	errors = {}
	_check_passwords(errors, pw1, pw2)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		_set_passwords(user, pw1, support_old)
		user.verified = True
		sess.add(user)
	
	return errors

def _set_passwords(user, pw, support_old):
	user.password = hash.hasher.encode(pw)
	if support_old:
		pw_md5 = hash.hasher_md5.encode(pw)
		user.set_front_data('msn', 'pw_md5', pw_md5)

def _send_password_reset_email(email, token):
	if settings.DEBUG:
		print("""********
Password reset requested for {}.
Here is your token:

\t{}

********""".format(email, token))
		return True
	
	import mailjet_rest
	mj = mailjet_rest.Client(auth = (settings.MAILJET['api_key'], settings.MAILJET['secret_key']))
	ret = mj.send.create({
		'FromName': "Escargot",
		'FromEmail': 'no-reply@escargot.log1p.xyz',
		'Subject': "Escargot password reset requested",
		'Recipients': [{ 'Email': email }],
		'Text-Part': RESET_TEMPLATE.format(email = email, token = token)
	})
	return ret.status_code == 200

RESET_TEMPLATE = """
A password reset was requested for the Escargot (https://escargot.log1p.xyz) account associated with {email}.

To change your password, follow this link within the next 60 minutes:

https://escargot.log1p.xyz/reset/{token}

If you did not request a password reset, ignore this email.
""".strip()

def _get_user(email):
	with Session() as sess:
		return sess.query(User).filter(User.email == email).one_or_none()

def _check_email(errors, email):
	if not (6 <= len(email) < 60) or ('@' not in email):
		# "Invalid email."
		errors['email'] = 'invalid'
	if '|' in email:
		# "Don't put your password in the email here; that's only when you log in to MSN < 5 or 7.5"
		errors['email'] = 'pass-in-email'

def _check_passwords(errors, pw1, pw2):
	if len(pw1) < 6:
		# "Password too short: 6 characters minimum."
		errors['password1'] = 'too-short'
	elif pw1 != pw2:
		# "Passwords don't match."
		errors['password2'] = 'no-match'

def render(req, tmpl, ctxt = None, status = 200):
	tmpl = req.app.jinja_env.get_template(tmpl)
	if ctxt is None:
		ctxt = {}
	ctxt['stats'] = req.app.stats
	content = tmpl.render(**ctxt)
	return web.Response(status = status, content_type = 'text/html', text = content)

PURPOSE_PWRESET = 'pwreset'
