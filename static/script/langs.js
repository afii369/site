(function (window) {
	// Current instructions shown
	var pShown;
	var rs;
	var sLang;
	var userLang;
	var site;
	
	function main() {
		userLang = (navigator.userLanguage || navigator.language).toLowerCase();
		if (userLang.indexOf('pt') == -1) {
			userLang = userLang.split('-')[0];
		}

		var pathname = window.location.pathname;
		
		switch (pathname) {
			case '/downloads':
				site = 'downloads';
				break;
			case '/patching':
				site = 'patching';
				break;
			case '/register':
				site = 'register';
				break;
			case '/forgot':
				site = 'forgot';
				break;
			default:
				site = 'home';
		}
		if (pathname.indexOf('/reset') != -1) {
			site = 'reset';
		}
		
		pShown = null;
		
		if (site == 'downloads') {
			setInterval(function () {
				if (window.pageYOffset > 10) {
					window.scrollTo(0, document.body.scrollHeight);
				} else {
					window.scrollTo(0, 0);
				}
			}, 500);
		}
		
		rs = document.getElementById('id').title;
		translate(userLang);
		
		sLang = getJson('translation/'+userLang, userLang)[site];
	}
	
	function showWarn(type) {
		var contactEmail = 'maigol@tuta.io';
		var contactSub = "?subject=Escargot%20site%20translation%20to%20'"+userLang+"'";
		var contactLink = "<a href=\"mailto:"+contactEmail+contactSub+"\">"+contactEmail+'</a>';
		var content;
		if (type == 'partial') {
			content = "This part of the website is not translated to your language (" + userLang + ") yet. If you want to help, please contact this email: " + contactLink;
		} else {
			content = "The site hasn't been translated to your language (" + userLang + ") yet. If you want to help, please contact this email: " + contactLink;
		}
		
		var d = document.createElement('div');
		d.style.textAlign = 'center';
		d.style.position = 'fixed';
		d.style.width = '100%';
		d.style.backgroundColor = 'orange';
		d.style.top = '5px';
		d.style.border = '2px solid #7a4f00';
		d.style.fontSize = '11px';
		d.innerHTML = content;
		document.body.appendChild(d);
	}
	
	function getJson(file) {
		var req = new XMLHttpRequest();
		req.open('GET', '/static/'+file+'.json', false);
		req.send();
		if (req.status == 200) {
			if (req.response != undefined) {
				return JSON.parse(req.response);
			}
		} else if (req.status == 404) {
			showWarn('full');
			translate('en');
		}
	}
	
	function translate(langcode) {
		var lang = getJson('translation/' + langcode);
		sLang = lang[site];
		
		if ('text' in sLang) {
			for (i in sLang.text) {
				try {
					document.getElementById(i).innerHTML = sLang['text'][i];
				} catch(e) {
					continue;
				}
			}
		}
		if ('xtras' in sLang) {
			for (t in sLang.xtras.titles) {
				document.getElementById('xtra-'+t+'-title').innerHTML = sLang.xtras.titles[t];
			}
			for (d in sLang.xtras.descs) {
				document.getElementById('xtra-'+d).title = sLang.xtras.descs[d];
			}
		}
		if ('status' in sLang) {
			for (i in sLang.status) {
				for (s in document.getElementsByClassName(i)) {
					var e = ['item', 'namedItem', 'length'];
					if (e.indexOf(s) == -1) {
						document.getElementsByClassName(i)[s].childNodes[0].textContent = sLang.status[i];
					}
				}
			}
		}
		if ('placeholders' in sLang) {
			for (p in sLang.placeholders) {
				document.getElementById(p).placeholder = sLang.placeholders[p]
			}
		}
		if (site == 'register' && document.getElementById('created_email') != null) {
			// Show registered account after translation was applied
			document.getElementById('created_email').innerHTML = document.getElementById('c-email-h').innerHTML;
		}
		if (site == 'forgot' && document.getElementById('sent_to') != null) {
			document.getElementById('sent_to').innerHTML = document.getElementById('c-email-s').innerHTML;
		}
		if ('errors' in sLang) {
			var errId;
			var errMsg;
			for (var e in sLang.errors) {
				if (document.getElementById(e+'-error') == null) continue;
				errId = e;
				errMsg = document.getElementById(e+'-error');
				break;
			}
			
			for (var m in sLang.errors[errId]) {
				if (errMsg.innerHTML != m) continue;
				errMsg.innerHTML = sLang.errors[errId][m];
			}
		}
		if (site == 'forgot' || site == 'reset') {
			for (p in lang.register.placeholders) {
				try {
					document.getElementById(p).placeholder = lang.register.placeholders[p];
				} catch(e) {}
			}
			if (document.getElementById('support_old-text') != null) {
				document.getElementById('support_old-text').innerHTML = lang.register.text['support_old-text'];
			}
		}
		
		if (lang.unfinished) {
			if (lang['unfinished'].indexOf(rs) != -1) {
				showWarn('partial', langcode);
			}
		}
	}
	
	function isMirrored(s, m) {
		for (var i in m) {
			if (m[i].indexOf(s) >= 0) return true;
		}
		return false;
	}
	
	function getDownloads(lang) {
		var gP = 'https://storage.googleapis.com/escargot-storage-1/public/';
		var eP = 'http://storage.log1p.xyz/';
		
		var d = getJson('json/downloads');
		var m = getJson('json/mirrored');
		
		for (var i in d.lang) {
			// This resets the href to avoid a link to a previously selected from being stored
			document.getElementById('up-'+i).href = '#';
			
			if (i >= 50) {
				document.getElementById('pp-'+i).href = '#';
			}
		}
		
		for (var i in d[lang]) {
			if (i >= 50) {
				var t = d[lang][i][0];
				var a = document.getElementById('pp-'+i);
				if (isMirrored(t, m)) {
					a.href = eP + 'patched-installer/' + t;
				} else {
					a.href = gP + 'patched-installer/' + t;
				}
			}
		}
		for (var i in d[lang]) {
			var t = d[lang][i][1];
			var a = document.getElementById('up-'+i);
			if (isMirrored(t, m)) {
				a.href = eP + 'msn-installer/' + t;
			} else {
				a.href = gP + 'msn-installer/' + t;
			}
		}
		document.getElementById('recommended').href = document.getElementById('pp-85').href;
		
		for (var i in d.lang) {
			var x = document.getElementById('up-'+i);
			if (x.href.indexOf('#') != -1) {
				x.style.display = 'none';
			} else {
				x.style.display = 'inline-block';
			}
			
			if (i >= 50) {
				var x = document.getElementById('pp-'+i);
				if (x.href.indexOf('#') != -1) {
					x.style.display = 'none';
				} else {
					x.style.display = 'inline-block';
				}
			}
		}
	}
	
	function showPatchingInstructions(ver) {
		if (pShown == ver) return;
		hideP(pShown);
		pShown = ver;
		document.getElementById('patch'+ver).style.display = 'block';
	}
	
	function hideP(ver) {
		if (ver == null) return;
		document.getElementById('patch'+ver).style.display = 'none';
	}
	
	function waitForDomLoaded(f) {
		if (document.addEventListener) {
			document.addEventListener('DOMContentLoaded', f);
		} else {
			setTimeout(f, 9);
		}
	}
	
	window.getDownloads = getDownloads;
	window.showPatchingInstructions = showPatchingInstructions;
	waitForDomLoaded(main);
})(window);
